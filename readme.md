# Valuebound CMS Backend

## Introduction

### Responses

| Code | Status                |
| ---- | --------------------- |
| 200  | Ok                    |
| 201  | Created               |
| 400  | Bad request           |
| 401  | UnAuthenctcated       |
| 403  | Forbiden access       |
| 404  | Not found             |
| 422  | Invalid input         |
| 500  | Internal server error |

## Installation steps

> git clone https://gitlab.com/amit_vb/vb_cms_backend.git

> cd vb_cms_backend

> cp .env.sample .env

> update env variable

> npm i

> npm start

## Work flow for git

### Create new branch and start working ...

> git checkout master

> git pull origin master

> git checkout -b your_branch_name

> Add/Modify code (work to your branch)

> git add .

> git commit -am "[AK] Ticket_no Task_name"

> git fech

> git merge origin/master

> git push origin your_branch_name

> create a pull request and ask teammates to review it. after you recive green signal (Pull request approved ) then plase select stash and merged

### Useing exiting branch

> git branch

> git checkout your_branch_name

> git fetch

> git merge orgin/master

> write your code and follow bellow steps

> Add/Modify code (work to your branch)

> git add .

> git commit -am "[AK] Ticket_no Task_name"

> git fech

> git merge origin/master

> git push origin your_branch_name

> create a pull request and ask teammates to review it. after you recive green signal (Pull request approved ) then plase select stash and merged
