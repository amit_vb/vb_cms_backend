const swaggerAutogen = require("swagger-autogen")();

const doc = {
  info: {
    title: "Valuebound CMS",
    description: `A Valuebound Headless CMS is a back-end only content management system (CMS) going to built from the ground up as a content repository that makes content accessible via a RESTful API for display on any device. The term headless comes from the concept of chopping the “head” (the front end, i.e. the website) off the body (the back end, i.e. the content repository). A headless CMS remains with an interface to manage content and a RESTful or GraphQL API to deliver content wherever you need it. Due to this approach, a headless CMS does not care about how and where your content gets displayed. A headless CMS has only one focus: storing and delivering structured content and allowing content editors to collaborate on new content`,
  },
  host: "localhost:3000",
  basePath: "/",
  schemes: ["http", "https"],
  consumes: ["application/json"],
  produces: ["application/json"],
  tags: [
    {
      name: "User",
      description: "Endpoints",
    },
    {
      name: "Schema",
      description: "Endpoints",
    },
    {
      name: "Content",
      description: "Endpoints",
    },
  ],
  // securityDefinitions: {
  //   apiKeyAuth: {
  //     type: "apiKey",
  //     in: "header",
  //     name: "X-API-KEY",
  //     description: "any description...",
  //   },
  // },
};

const outputFile = "./public/api-docs/swagger-output.json";
const endpointsFiles = ["./src/routes/index.js"];
swaggerAutogen(outputFile, endpointsFiles, doc);
