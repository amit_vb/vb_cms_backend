const mongoose = require("mongoose");

const contentSchema = new mongoose.Schema({
  author: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  slug: {
    type: String,
    required: true,
  },
  form_data: {
    type: Object,
    required: true,
  },
});

const ContentModel = mongoose.model("Content", contentSchema);

module.exports = ContentModel;
