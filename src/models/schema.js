const mongoose = require("mongoose");

const SchemaSchema = new mongoose.Schema({
 
  
    title: {
      type: String,
      required: true,
    },
    slug: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true,
    },
    properties: {
      type: Object,
      required: true,
    },
  
});

const schemaModel = mongoose.model("schema", SchemaSchema);

module.exports = schemaModel;
