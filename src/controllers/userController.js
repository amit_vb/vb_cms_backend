const Joi = require("joi");
const jwt = require("jsonwebtoken");
const { addUserSchema } = require("../schema/userSchema");
const { customResponse, customPagination } = require("../utility/helper");

const userModel = require("../models/user");

const getUserList = async (req, res) => {
  /* 	#swagger.tags = ['User']
      #swagger.description = 'Get users list' 
      #swagger.parameters['page'] = {
        in: 'query',
        type: 'integer',
        description: 'Page number' 
      }
      #swagger.parameters['limit'] = {
        in: 'query',
        type: 'integer',
        description: 'Data limit per page' 
      }
  */
  let code, message;
  const page = req.query.page ? req.query.page : 1;
  const limit = req.query.limit ? req.query.limit : 15;
  try {
    code = 200;
    const users = await userModel.find({});
    const data = customPagination({ data: users, page, limit });
    const resData = customResponse({ code, data });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
};

const getUserDeatil = async (req, res) => {
  /* 	#swagger.tags = ['User']
   #swagger.description = 'Get users Detail' 
  */
  let code, message;
  const _id = req.params.id;
  try {
    code = 200;
    const data = await userModel.findById({ _id });
    const resData = customResponse({ code, data });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
};

const addUser = async (req, res) => {
  /* 	#swagger.tags = ['User']
      #swagger.description = 'Add new user'
      #swagger.parameters['obj'] = {
        in: 'body',
        schema: {
            $name: 'Jhon Doe',
            $email: 'jhon@valuebound.com',
            $role: 'admin'
        }
      }
      #swagger.responses[201] = {
        description: 'User successfully added.',
        schema: { 
          "status": "success",
          "code": 201,
          "message": "",
          "data": {
            "name": 'Jhon Doe',
            "email": 'jhon@valuebound.com',
            "role": 'admin', 
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiSmhvbiBEb2UiLCJpYXQiOjE2Mjg0OTQ5NzksImV4cCI6MTYyODY2Nzc3OSwiaXNzIjoidmItY21zIn0.wdyX_wXWABr1BIw_7FzZKgowhixX8EXVN4ZojvzsaIU",
          },
          "error": {}
        }
      }
  */
  let code, message;
  const { error } = addUserSchema.validate(req.body);
  if (error) {
    code = 422;
    message = "Invalid request data";
    const resData = customResponse({
      code,
      message,
      err: error && error.details,
    });
    return res.status(code).send(resData);
  }
  try {
    code = 201;
    const user = new userModel(req.body);
    await user.save();
    const payload = { user: user.name };
    const options = {
      expiresIn: process.env.EXPIRESIN,
      issuer: process.env.ISSUER,
    };
    const secret = process.env.JWT_SECRET;
    const token = jwt.sign(payload, secret, options);
    const data = { token, ...user };
    const resData = customResponse({
      code,
      data,
    });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
};

const updateUser = async (req, res) => {
  /* 	#swagger.tags = ['User']
      #swagger.description = 'Update user' 
      #swagger.parameters['obj'] = {
        in: 'body',
        schema: {
            $name: 'Jhon Doe',
            $email: 'jhon@valuebound.com',
            $role: 'admin'
        }
      }
      #swagger.responses[200] = {
        description: 'User successfully updated.',
        schema: { 
          "status": "success",
          "code": 200,
          "message": "",
          "data": {
            "name": 'Jhon Doe',
            "email": 'jhon@valuebound.com',
            "role": 'admin'
          },
          "error": {}
        }
      }
  */
  let code, message;
  const _id = req.params.id;
  try {
    code = 200;
    const user = await userModel.findOneAndUpdate(
      { _id },
      { ...req.body },
      { new: true }
    );
    await user.save();
    const resData = customResponse({
      code,
      data: user,
    });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
};

const deleteUser = async (req, res) => {
  /* 	#swagger.tags = ['User']
   #swagger.description = 'Delete user' 
  */
  let code, message;
  const _id = req.params.id;
  try {
    code = 200;
    const user = await userModel.findByIdAndDelete({ _id });
    const resData = customResponse({
      code,
      data: user,
    });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
};

module.exports = {
  getUserList,
  getUserDeatil,
  addUser,
  updateUser,
  deleteUser,
};
