const Joi = require("joi");
const jwt = require("jsonwebtoken");
const { addSchemaSchema } = require("../schema/schemaSchema");
const { customResponse, customPagination } = require("../utility/helper");

const schemaModel = require("../models/schema");

const getSchemaList = async (req, res) => {
  /* 	#swagger.tags = ['Schema']
      #swagger.description = 'Get schemas list' 
      #swagger.parameters['page'] = {
        in: 'query',
        type: 'integer',
        description: 'Page number' 
      }
      #swagger.parameters['limit'] = {
        in: 'query',
        type: 'integer',
        description: 'Data limit per page' 
      }
  */
  let code, message;
  const page = req.query.page ? req.query.page : 1;
  const limit = req.query.limit ? req.query.limit : 15;
  try {
    code = 200;
    const users = await schemaModel.find({});
    const data = customPagination({ data: users, page, limit });
    const resData = customResponse({ code, data });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
};

const getSchemaDetail = async (req, res) => {
  /* 	#swagger.tags = ['Schema']
   #swagger.description = 'Get schemas Detail' 
  */
  let code, message;
  const _id = req.params.id;
  try {
    code = 200;
    const data = await schemaModel.findById({ _id });
    const resData = customResponse({ code, data });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
};

const addSchema = async (req, res) => {
  /* 	#swagger.tags = ['Schema']
      #swagger.description = 'Add new schema'
      #swagger.parameters['obj'] = {
        in: 'body',
        schema: {
          "title":"Valuebound",
          "slug":"the-slug",
          "type":"thetype",
          "properties":{
              "title":"Valuebound",
          "slug":"the-slug",
          "type":"thetype"
          }
}
      }
      #swagger.responses[201] = {
        description: 'Schema successfully added.',
        schema: {
    "status": "success",
    "code": 201,
    "message": "",
    "data": {
        "_id": "611242ffcddb8d4d608a2cff",
        "title": "Valuebound",
        "slug": "the-slug",
        "type": "thetype",
        "properties": {
            "title": "Valuebound",
            "slug": "the-slug",
            "type": "thetype"
        },
        "__v": 0
    },
    "error": {}
    }
           
      }
  */
  let code, message;
  const { error } = addSchemaSchema.validate(req.body);
  if (error) {
    code = 422;
    message = "Invalid request data";
    const resData = customResponse({
      code,
      message,
      err: error && error.details,
    });
    return res.status(code).send(resData);
  }
  try {
    code = 201;
    const data = new schemaModel(req.body);
    await data.save();
    const resData = customResponse({
      code,
      data,
    });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
};

const updateSchema = async (req, res) => {
  /* 	#swagger.tags = ['Schema']
      #swagger.description = 'Update schema' 
      #swagger.parameters['obj'] = {
        in: 'body',
        schema: {
          "title":"Valuebound",
          "slug":"the-slug",
          "type":"the good type",
          "properties":{
              "title":"Valuebound",
              "slug":"the-slug",
              "type":"the good type"
            }
        }
      }
      #swagger.responses[200] = {
        description: 'Schema successfully updated.',
        schema: {
            "status": "success",
            "code": 200,
            "message": "",
            "data": {
                "_id": "61120de993e64227b4aa1924",
                "title": "Valuebound",
                "slug": "the-slug",
                "type": "the good type",
                "properties": {
                    "title": "Valuebound",
                    "slug": "the-slug",
                    "type": "the good type"
                },
                "__v": 0
            },
            "error": {}
        }
      }
  */
  let code, message;
  const _id = req.params.id;
  try {
    code = 200;
    const user = await schemaModel.findOneAndUpdate(
      { _id },
      { ...req.body },
      { new: true }
    );
    await user.save();
    const resData = customResponse({
      code,
      data: user,
    });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
};

const deleteSchema = async (req, res) => {
  /* 	#swagger.tags = ['Schema']
   #swagger.description = 'Delete schema'
   #swagger.responses[200] = {
        description: 'Schema successfully deleted.'
   } 
  */
  let code, message;
  const _id = req.params.id;
  try {
    code = 200;
    const user = await schemaModel.findByIdAndDelete({ _id });
    const resData = customResponse({
      code,
      data: user,
    });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
};

module.exports = {
  getSchemaList,
  getSchemaDetail,
  addSchema,
  updateSchema,
  deleteSchema,
};
