const { addContentSchema } = require("../schema/contentSchema");
const { customResponse, customPagination } = require("../utility/helper");
const Content = require("../models/content");

const addContent = async (req, res) => {
  /* 	#swagger.tags = ['Content']
      #swagger.description = 'Add new content'
      #swagger.parameters['obj'] = {
        in: 'body',
        schema: {
          $author : "sai",
          $type  : "test",
          $title  : "demo",
          $slug   : "demo",
          $form_data : {"json":"json"}
        }
      }
  */
  let code, message;
  const { error } = addContentSchema.validate(req.body);
  if (error) {
    code = 422;
    message = "Invalid request data";
    const resData = customResponse({
      code,
      message,
      err: error && error.details,
    });
    return res.status(code).send(resData);
  }
  try {
    code = 201;
    const content = new Content(req.body);
    await content.save();
    console.log(content)
    const resData = customResponse({
      code,
      data: content,
    });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
};

const getContent = async (req, res) => {
  /* 	
      #swagger.tags = ['Content']
      #swagger.description = 'Get content list' 
      #swagger.parameters['page'] = {
        in: 'query',
        type: 'integer',
        description: 'Page number' 
      }
      #swagger.parameters['limit'] = {
        in: 'query',
        type: 'integer',
        description: 'Data limit per page' 
      }
  */
  let code, message;
  const page = req.query.page ? req.query.page : 1;
  const limit = req.query.limit ? req.query.limit : 15;
  try {
    code = 200;
    const content = await Content.find({});
    const data = customPagination({ data: content, page, limit });
    const resData = customResponse({ code, data });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    console.log(error)
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
}

const getContentDetail = async (req, res) => {
  /*
    #swagger.tags = ['Content']
    #swagger.description = 'Get content Detail' 
  */
  let code, message;
  const _id = req.params.id;
  console.log(_id)
  try {
    code = 200;
    const data = await Content.findById({ _id });
    const resData = customResponse({ code, data });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
};

const deleteContent = async (req, res) => {
  /* 	#swagger.tags = ['Content']
   #swagger.description = 'Delete content' 
  */
  let code, message;
  const _id = req.params.id;
  try {
    code = 200;
    const user = await Content.findByIdAndDelete({ _id });
    const resData = customResponse({
      code,
      data: user,
    });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
};

const updateContent = async (req, res) => {
  /*
      #swagger.tags = ['Content']
      #swagger.description = 'Update content' 
      #swagger.parameters['obj'] = {
        in: 'body',
        schema: {
          $author : "prabas",
          $type  : "test",
          $title  : "demo",
          $slug   : "demo",
          $form_data : {"json":"json"}
        }
      }
  */
  let code, message;
  const _id = req.params.id;
  try {
    code = 200;
    const user = await Content.findOneAndUpdate(
      { _id },
      { ...req.body },
      { new: true }
    );
    await user.save();
    const resData = customResponse({
      code,
      data: user,
    });
    return res.status(code).send(resData);
  } catch (error) {
    code = 500;
    message = "Internal server error";
    const resData = customResponse({
      code,
      message,
      err: error,
    });
    return res.status(code).send(resData);
  }
}

module.exports = {
  addContent,
  getContent,
  getContentDetail,
  deleteContent,
  updateContent
};
