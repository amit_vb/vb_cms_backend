var express = require("express");
var router = express.Router();

const { addContent,getContent,getContentDetail,deleteContent,updateContent } = require("../controllers/contentController");
const { validateFormData } = require("../middleware/validateFormData");

router.post("/", validateFormData, addContent);
router.get("/", getContent);
router.get("/:id", getContentDetail);
router.delete("/:id", deleteContent);
router.put("/:id", updateContent);

module.exports = router;
