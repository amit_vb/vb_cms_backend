var express = require("express");
var router = express.Router();

const { isAuthorized } = require("../middleware/auth");
const {
  getSchemaList,
  getSchemaDetail,
  addSchema,
  updateSchema,
  deleteSchema,
} = require("../controllers/schemaController");

router.get("/", getSchemaList);
router.get("/:id", getSchemaDetail);
router.post("/", addSchema);
router.put("/:id", updateSchema);
router.delete("/:id", deleteSchema);

module.exports = router;
