var express = require("express");
var router = express.Router();

const userRoutes = require("./users");
const contentRoutes = require("./content");

const schemaRoutes = require("./schema");
router.use("/users", userRoutes);
router.use("/content", contentRoutes);
router.use("/schemas", schemaRoutes);


module.exports = router;
