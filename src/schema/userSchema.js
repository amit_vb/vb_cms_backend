const Joi = require("joi");

const addUserSchema = Joi.object()
  .keys({
    name: Joi.string().min(3).max(30).required(),
    email: Joi.string().min(3).max(30).required(),
    role: Joi.string().min(3).max(30).required(),
    
  })
  .options({ abortEarly: false });

module.exports = { addUserSchema };
