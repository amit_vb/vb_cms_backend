const Joi = require("joi");

const addContentSchema = Joi.object()
  .keys({
  author:Joi.string().alphanum().min(3).max(255).required(),
  type:Joi.string(),
  title:Joi.string(),
  slug:Joi.string(),
  form_data: Joi.object()
})

module.exports = {addContentSchema};
