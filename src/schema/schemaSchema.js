const Joi = require("joi");

const addSchemaSchema = Joi.object()
  .keys({
    title: Joi.string().min(3).max(150).required(),
    slug: Joi.string().min(3).max(30).required(),
    type: Joi.string().min(3).max(30).required(),
    properties: Joi.object().required(),
  })
  .options({ abortEarly: false });

module.exports = { addSchemaSchema };
